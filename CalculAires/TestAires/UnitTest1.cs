﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CalculAires;

    namespace TestAires
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestAireCarre()
        {
            Assert.AreEqual(4, Program.AireCarre(2));
            Assert.AreEqual(1, Program.AireCarre(1));
            Assert.AreEqual(6.25, Program.AireCarre(2.5));

        }
        [TestMethod]
        public void TestAireRectangle()
        {
            Assert.AreEqual(4, Program.AireRectangle(2, 2));
            Assert.AreEqual(3, Program.AireRectangle(1, 3));
            Assert.AreEqual(100, Program.AireRectangle(25, 4));
            Assert.AreEqual(32.061009735, Program.AireRectangle(4.3335, 7.39841));
        }
        [TestMethod]
        public void TestAireTriangle()
        {
            Assert.AreEqual(2, Program.AireTriangle(2, 2));
            Assert.AreEqual(5, Program.AireTriangle(10, 1));
            Assert.AreEqual(3.75, Program.AireTriangle(15, 0.5));
            Assert.AreEqual(0.46875, Program.AireTriangle(3.75, 0.25));
        }
        [TestMethod]
        public void TestPersoMajeur()
        {
            Assert.AreEqual(true, Program.PersoMajeur(2000));
            Assert.AreEqual(true, Program.PersoMajeur(1952));
            Assert.AreEqual(false, Program.PersoMajeur(2200));
        }
        [TestMethod]
        public void TestEntierPresent()
        {
            Assert.AreEqual(true, Program.EntierPresent(new int[] {0, 1, 2, 3, 4, 5}, 5));
            Assert.AreEqual(false, Program.EntierPresent(new int[] { 0, 1, 2, 3, 4, 5 }, 7));
        }
        [TestMethod]
        public void TestStringPresente()
        {
            Assert.AreEqual(true, Program.StringPresente(new string[] { "Prof", "Sami", "Delahaye", "Aime", "Les", "Oreilles", "De", "Chats" }, "Sami"));
            Assert.AreEqual(false, Program.StringPresente(new string[] { "" }, "Sami"));
        }
        [TestMethod]
        public void TestPositionEntier()
        {
            Assert.AreEqual(5, Program.EntierPresent(new int[] { 0, 1, 2, 3, 4, 5 }, 5));
            Assert.AreEqual(-1, Program.EntierPresent(new int[] { 0, 1, 2, 3, 4, 5 }, 7));
        }
        [TestMethod]
        public void TestPositionString()
        {
            Assert.AreEqual(1, Program.StringPresente(new string[] { "Prof", "Sami", "Delahaye", "Aime", "Les", "Oreilles", "De", "Chats" }, "Sami"));
            Assert.AreEqual(-1, Program.StringPresente(new string[] { "" }, "Sami"));
        }
        [TestMethod]
        public void TestMultiplicationTableau()
        {
            Assert.AreEqual(new int[] { 0, 2, 4, 6, 8, 10 }, Program.EntierPresent(new int[] { 0, 1, 2, 3, 4, 5 }, 2));
        }
    }
}
