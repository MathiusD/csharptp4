﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculAires
{
    public class Program
    {
        public static double AireCarre(double cote)
        {
            return cote * cote;
        }
        public static double AireRectangle(double largeur, double longueur)
        {
            return largeur * longueur;
        }
        public static double AireTriangle(double basetriangle, double hauteur)
        {
            return (basetriangle * hauteur) / 2;
        }
        public static bool PersoMajeur(int anneenais)
        {
            bool retour = false;
            if (DateTime.Now.Year >= (anneenais + 18))
            {
                retour = true;
            }
            return retour;
        }
        public static bool EntierPresent(int[] tableau, int waldo)
        {
            bool retour = false;
            for (int indice=0; indice<(tableau.Length); indice++)
            {
                if (waldo == tableau[indice])
                {
                    retour = true;
                }
            }
            return retour;
        }
        public static bool StringPresente(string[] tableau, string waldo)
        {
            bool retour = false;
            for (int indice = 0; indice < (tableau.Length); indice++)
            {
                if (waldo == tableau[indice])
                {
                    retour = true;
                }
            }
            return retour;
        }
        public static int PositionEntier(int[] tableau, int waldo)
        {
            int retour = -1;
            for (int indice = 0; indice < (tableau.Length); indice++)
            {
                if (waldo == tableau[indice])
                {
                    retour = indice;
                }
            }
            return retour;
        }
        public static int PositionString(string[] tableau, string waldo)
        {
            int retour = -1;
            for (int indice = 0; indice < (tableau.Length); indice++)
            {
                if (waldo == tableau[indice])
                {
                    retour = indice;
                }
            }
            return retour;
        }
        public static int[] MultiplicationTableau(int[] tableau, int waldo)
        {
            for (int indice = 0; indice < (tableau.Length); indice++)
            {
                tableau[indice] = tableau[indice] * waldo;
            }
            return tableau;
        }
        static void Main(string[] args)
        {
        }
    }
}
